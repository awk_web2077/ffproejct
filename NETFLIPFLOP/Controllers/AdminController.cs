﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NETFLIPFLOP.Models;

namespace NETFLIPFLOP.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        private Entities1 db = new Entities1();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Income()
        {
            List<INCOME> ics = new List<INCOME>();
            foreach(INCOME ic in db.INCOME)
            {
                ics.Add(ic);
            }
            return View("Income", ics);
        }
    }
}