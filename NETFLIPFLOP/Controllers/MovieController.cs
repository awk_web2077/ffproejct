﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NETFLIPFLOP.Models;
using System.Net;

namespace NETFLIPFLOP.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie
        private Entities1 db = new Entities1();

        public ActionResult Index(int? id, string sort)
        {
            List<MOVIE> movies = new List<MOVIE>();
            List<MOVIE_TYPE> mtype = new List<MOVIE_TYPE>();
            List<MOVIE> find = new List<MOVIE>();
            foreach (MOVIE m in db.MOVIE)
            {
                movies.Add(m);
            }
            foreach (MOVIE_TYPE mt in db.MOVIE_TYPE)
            {
                mtype.Add(mt);

            }
            switch (id)
            {
                case 1:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Action")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 2:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Adventure")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 3:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Sci-Fi")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 4:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Horror")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 5:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Comedy")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 6:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Thriller")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 7:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Fantasy")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 8:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Romance")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 9:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Crime")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 10:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Animation")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                case 11:
                    foreach (MOVIE_TYPE mt in mtype)
                    {
                        if (mt.TYPE == "Drama")
                        {
                            find.Add(db.MOVIE.Find(mt.ID));
                        }
                    }
                    break;
                default:
                    find = movies;
                    break;
            }
            switch (sort)
            {
                case "name":
                    find = find.OrderBy(i => i.NAME).ToList();
                    break;
                case "Score":
                    find = find.OrderByDescending(i => i.SCORE).ToList();
                    break;
                case "Arrive":
                    find = find.OrderByDescending(i => i.DATE_ARRIVE).ToList();
                    break;
                default:
                    find = find.OrderBy(i => i.NAME).ToList();
                    break;
            }
            return View("Movie", find);
        }

        public ActionResult Movie(int? id)
        {
            return RedirectToAction("Index/" + id.ToString());
        }

        public ActionResult Watch(int? id)
        {
            MOVIE m = db.MOVIE.Find(id);
            return View(m);
        }

        [NonAction]
        public bool IsPackageExpired()
        {
            CUSTOMER cus= new CUSTOMER();
            foreach(CUSTOMER c in db.CUSTOMER)
            {
                if (c.USERNAME == User.Identity.Name)
                {
                    cus = c;
                    break;
                }
            }
            DateTime dt = new DateTime(cus.EXP_DATE.Ticks);
            DateTime now = DateTime.Now;
            if (now.Subtract(dt).Days / (365.25 / 12) >= 1)
                return true;
            return false;
        }
    }
}