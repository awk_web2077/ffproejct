﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NETFLIPFLOP.Models;

namespace NETFLIPFLOP.Controllers
{
    public class MovieAdminController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: MovieAdmin
        public ActionResult Index()
        {
            return View(db.MOVIE.ToList());
        }

        // GET: MovieAdmin/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOVIE mOVIE = db.MOVIE.Find(id);
            if (mOVIE == null)
            {
                return HttpNotFound();
            }
            return View(mOVIE);
        }

        // GET: MovieAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MovieAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NAME,LICENSE_COST,SCORE,DATE_ARRIVE,POSTERURL,TAG,VIDEOURL")] MOVIE mOVIE)
        {
            if (ModelState.IsValid)
            {
                int id = 0;
                foreach (MOVIE m in db.MOVIE)
                {
                    if(m.ID > id)
                    {
                        id = m.ID;
                    }
                }
                mOVIE.ID = id + 1;
                db.MOVIE.Add(mOVIE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mOVIE);
        }

        // GET: MovieAdmin/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOVIE mOVIE = db.MOVIE.Find(id);
            if (mOVIE == null)
            {
                return HttpNotFound();
            }
            return View(mOVIE);
        }

        // POST: MovieAdmin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NAME,LICENSE_COST,SCORE,DATE_ARRIVE,POSTERURL,TAG,VIDEOURL")] MOVIE mOVIE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mOVIE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mOVIE);
        }

        // GET: MovieAdmin/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MOVIE mOVIE = db.MOVIE.Find(id);
            if (mOVIE == null)
            {
                return HttpNotFound();
            }
            return View(mOVIE);
        }

        // POST: MovieAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MOVIE mOVIE = db.MOVIE.Find(id);
            db.MOVIE.Remove(mOVIE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
