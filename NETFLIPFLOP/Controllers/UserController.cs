﻿using NETFLIPFLOP.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NETFLIPFLOP.Controllers
{
    public class UserController : Controller
    {
        private Entities1 db = new Entities1();

        // GET: CUSTOMERs
        public ActionResult Index()
        {
            var cUSTOMER = db.CUSTOMER.Include(c => c.INCOME);
            return View(cUSTOMER.ToList());
        }

        // GET: CUSTOMERs/Create
        public ActionResult Create()
        {
            ViewBag.ID = new SelectList(db.INCOME, "ID", "PACKAGE");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,PASSWORD,FNAME,LNAME,USERNAME,PACKAGE,PAPERPAL,EXP_DATE,EMAIL")] CUSTOMER cUSTOMER)
        {
            if (ModelState.IsValid)
            {
                // ID
                int id = 0;
                if (db.CUSTOMER.Count() == 0)
                {
                    id = 1;
                }
                else
                {
                    foreach (CUSTOMER c in db.CUSTOMER)
                    {
                        if (c.ID > id)
                        {
                            id = c.ID;
                        }
                    }
                    cUSTOMER.ID = id + 1;
                }
                //check Username
                foreach(CUSTOMER c in db.CUSTOMER)
                {
                    if(c.USERNAME == cUSTOMER.USERNAME)
                       return RedirectToAction("User","Error");
                }
                foreach (CUSTOMER c in db.CUSTOMER)
                {
                    if (c.EMAIL == cUSTOMER.EMAIL)
                        return RedirectToAction("Email", "Error");
                }
                foreach (CUSTOMER c in db.CUSTOMER)
                {
                    if (c.PAPERPAL == cUSTOMER.PAPERPAL)
                        return RedirectToAction("PaperPal", "Error");
                }
                // EXP_DATE
                DateTime dt = new DateTime(DateTime.Now.Ticks);
                dt = dt.AddDays(30);
                cUSTOMER.EXP_DATE = dt;
                ///////////////////////
                db.CUSTOMER.Add(cUSTOMER);
                db.SaveChanges();
                FormsAuthentication.SetAuthCookie(cUSTOMER.USERNAME, false);
                return RedirectToAction("Movie", "Movie");
            }

            ViewBag.ID = new SelectList(db.INCOME, "ID", "PACKAGE", cUSTOMER.ID);
            return View(cUSTOMER);
        }

        // GET: CUSTOMERs/Edit/5
        public ActionResult Edit()
        {
            if (User.Identity.IsAuthenticated)
            {
                foreach (CUSTOMER c in db.CUSTOMER)
                {
                    if (c.USERNAME == User.Identity.Name)
                    {
                        return View("Edit", c);
                    }
                }
            }
            return RedirectToAction("LogIn");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PASSWORD,FNAME,LNAME,USERNAME,PACKAGE,PAPERPAL,EXP_DATE,EMAIL")] CUSTOMER cUSTOMER)
        {

            if (ModelState.IsValid)
            {
                if (cUSTOMER.PACKAGE!="")
                {
                    cUSTOMER.EXP_DATE = DateTime.Now;
                    cUSTOMER.EXP_DATE = cUSTOMER.EXP_DATE.AddDays(30);
                }
                db.Entry(cUSTOMER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index","Movie");
            }
            ViewBag.ID = new SelectList(db.INCOME, "ID", "PACKAGE", cUSTOMER.ID);
            return View(cUSTOMER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult LogIn()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "MOVIE");
            else
                return View("LogIn");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn([Bind(Include = "ID,PASSWORD,FNAME,LNAME,USERNAME,PACKAGE,PAPERPAL,EXP_DATE,EMAIL")] CUSTOMER cUSTOMER)
        {
            foreach (CUSTOMER c in db.CUSTOMER)
            {
                if (c.USERNAME == cUSTOMER.USERNAME && c.PASSWORD == cUSTOMER.PASSWORD)
                {
                    FormsAuthentication.SetAuthCookie(cUSTOMER.USERNAME, false);
                    return RedirectToAction("Index", "Movie");
                }
            }
            return RedirectToAction("LogIn");
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("LogIn");
        }
    }
}
